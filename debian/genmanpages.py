#!/usr/bin/python3
from __future__ import print_function
from cnvlib import commands
import os
import sys
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess

for subcommand in commands.AP_subparsers._get_subactions():
    name = subcommand.dest
    description = subcommand.help
    if name == 'version':
        break
    with open("debian/cnvkit-%s.man.include" % name, "w") as f:
        print("[NAME]", file=f)
        print("cnvkit_%s \- %s" % (name, description), file=f)
    subprocess.check_call(['help2man', '--no-info', '--help-option="-h"', '--version-string=%s'
            % sys.argv[1], '--include=debian/cnvkit-%s.man.include' % name,
            '--output=debian/man/cnvkit-%s.1' % name, 'cnvkit.py %s' % name])
